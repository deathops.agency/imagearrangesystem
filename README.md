# Image arrange system for the scimage frame

### General Idea: 

We want to arrange a set of images from a bigger pool of images to show them.  
In general the order in which Iimages are displayed is in arbitrary order and controlled by fim. 
(See ansible/roles/scimage_frame/files/fim.service)  

The idea is to have a basic folder with all images in it, called the sync_folder (this folder is in sync with the scimage 
backend folder) and a folder called show_this used by fim command. 
So the "sync_folder" and the "show_this" folder are decoupled. 
The idea is to just symlink a set of images into "show_this" folder to show these images.

Any complex tasks e.g. show images with certain tags or images of a certain day are handled by a high level program 
like the "image arrange system" introduced here.  

One of the benefits is a fast sync without deep inspection of folder or recursive searches for filesystem changes. 
Another one is: The interface is symlinking images into an folder. 
todo: control of image order/arbitrary order has to be configurable. Then one can generate the order by symlink addons in the name. 




