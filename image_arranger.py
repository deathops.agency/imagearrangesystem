import pathlib
import datetime
from os import listdir
from os import symlink
from os import path
from os import remove
from os.path import isfile, join
from pydbus import SystemBus


class imageArranger():
    def __init__(self, source_path, target_path):
        self.__source_path = source_path
        self.__target_path = target_path

    def run(self):
        file_list = self.get_file_list(self.__source_path)
        todays_files = self.get_todays_files(file_list)
        current_images = self.get_file_list(self.__target_path)

        #there are new files today
        if bool(todays_files):
            #is any of the new files shown already? If not remove all files and add new files. If there are no new images in the list, remove all images.
            if not bool([value for value in todays_files if value in current_images]):
                self.remove_files(current_images, self.__target_path)
            self.create_image_links(todays_files)
        else:
            self.remove_files(current_images, self.__target_path)
            self.create_image_links(file_list)

    def get_file_list(self, path):
        image_file_paths = [f for f in listdir(path) if isfile(join(path, f))]
        return image_file_paths

    def get_todays_files(self, file_list):
        todays_new_files = []
        today = datetime.datetime.today().date()
        for file_name in file_list:
            source_file_path = self.__source_path + "/" + file_name
            modification_date = self.get_modification_date(source_file_path)
            if today == modification_date.date():
                todays_new_files.append(file_name)
        return todays_new_files

    def remove_files(self, files_to_remove, path):
        for file in files_to_remove:
            remove(path + "/" + file)

    def create_image_links(self, file_list):
        link_created = False
        for file in file_list:
            source_file = self.__source_path + "/" + file
            target_file = self.__target_path + "/" + file
            if not path.exists(target_file):
                symlink(source_file, target_file)
                link_created = True
        if link_created:
            self.restart_fim_service()

    def restart_fim_service(self):
        bus = SystemBus()
        systemd = bus.get(".systemd1")
        job1 = systemd.RestartUnit("fim.service", "replace")

    def get_modification_date(self, file_path):
        file_name = pathlib.Path(file_path)
        mtime = datetime.datetime.fromtimestamp(file_name.stat().st_mtime)
        return mtime

if __name__ == "__main__":
    # execute only if run as a script
    arr = imageArranger("/home/scimagesync/sync_folder", "/home/scimagesync/show_this")
    arr.run()
